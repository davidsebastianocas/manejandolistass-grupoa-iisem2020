/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.coleciones_seed;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class ListaS<T> {

    private Nodo<T> cabeza = null;
    private int cardinalidad = 0;

    public ListaS() {
    }

    /**
     * 1. Crear el nodo 2. Insertar el info en el nodo 3. El nuevo nodo su
     * siguiente es cabeza 4. Cabeza es ahora quién ? Nuevo nodo 5. aumentar
     * carnidalidad
     *
     * @param info que se desea almacenar
     */
    public void insertarInicio(T info) {
        // Paso 1.
        Nodo<T> nuevo = new Nodo();
        //Paso 2. 
        nuevo.setInfo(info);
        //Paso 3.
        nuevo.setSig(this.cabeza);
        //Paso 4.
        this.cabeza = nuevo;
        //Paso 5.
        this.cardinalidad++;
    }

    // arraylist x= .... , x.add(marco), x.add(juan) --> cabeza: marco
    // arraylist x= .... , x.add(juan),x.add(marco),  --> cabeza: juan   --> "método add"
    public int getCardinalidad() { //getTamanio o size()
        return cardinalidad;
    }

    @Override
    public String toString() {

        String msg = "Cab->";

        for (Nodo<T> posicion = this.cabeza; posicion != null; posicion = posicion.getSig()) {
            msg += posicion.getInfo().toString() + "->";
        }

        /*
        
        NO -->
        
            while(this.cabeza!=null)
        {   
        
                msg+=this.cabeza.getinfo().tostrng()
        cabeza=cabeza.getsig();
        }
         */
        return msg + "null";

    }

    public boolean esta(T info) // containtTo(..)
    {

        for (Nodo<T> posicion = this.cabeza; posicion != null; posicion = posicion.getSig()) {
            if (posicion.getInfo().equals(info)) {
                return true;
            }
        }
        return false;
    }

    public void insertarFinal(T info) {
        /**
         *
         * 1. crear nodo 2. info a nodo 3. Encontrar el último 4. ultimo su
         * siguiente es nuevo 5. nuevo su siguiente es null 6. Aumentar
         * cardinalidad
         */

        //Situación 1:
        if (this.esVacia()) {
            this.insertarInicio(info);
        } else {

            try {
                //1.
                Nodo<T> nuevo = new Nodo();
                //2. 
                nuevo.setInfo(info);
                //3. Encontrar el último --> posición cardinalidad-1
                Nodo<T> ultimo = this.getPos(cardinalidad - 1);
                //4. 
                ultimo.setSig(nuevo);
                //5. nuevo su siguiente es null
                nuevo.setSig(null);
                //6. 
                this.cardinalidad++;
            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
        }
    }

    /**
     * Obtiene el elemento almacenado en la posición pos
     *
     * @param pos el índice
     * @return un objeto
     */
    public T get(int pos) {

        try {
            Nodo<T> actual = this.getPos(pos);
            return actual.getInfo();

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            return null;
        }
    }

    /**
     * Actualiza un nuevo objeto en la posición pos
     *
     * @param pos índice para actualizar elemento
     * @param nuevo objeto nuevo
     */
    public void set(int pos, T nuevo) {

        try {
            Nodo<T> actual = this.getPos(pos);
            actual.setInfo(nuevo);

        } catch (Exception ex) {
            System.err.println(ex.getMessage());

        }
    }

    private Nodo<T> getPos(int posFin) throws Exception {
        if (this.esVacia() || posFin > this.cardinalidad || posFin < 0) {
            throw new Exception("La posición:" + posFin + " no es válida");
        }

        Nodo<T> x = this.cabeza;
        while (posFin > 0) {
            x = x.getSig();
            posFin--;
        }

        return x;
    }

    public boolean esVacia() {
        return this.cabeza == null;
    }

    public void borrarTodo() {
        //Dereferenciar:
        this.cabeza = null;
        this.cardinalidad = 0;
    }

    
    /**
     * Inserta un dato en la lista si y solo si , el dato NO está en ella
     * Condición: Sólo se debe tener un ciclo
     */
    /**
     * Método que a través de la posición del objeto , es borrrado de la lista
     * simple
     *
     * @param pos una posición válida en la lista (índice)
     * @return el objeto que estaba en esa posición
     */
    public T eliminar(int pos) {
        //1.Cuando la lista está vacía  No puedo borrar
        if (this.esVacia()) {
            return null;
        }
        /**
         * Cuando pos=0 -Borrar cabeza -Cabeza nueva estará en el siguiente
         * -Borrar referencia de nodo borrado, es decir, DESUNIRLO. -Actualizar
         * cardinalidad
         *
         */
        Nodo<T> borrar; //referenciar 
        if (pos == 0) {
            borrar = this.cabeza;
            this.cabeza = this.cabeza.getSig();

        } else {
            try {
                /**
                 * Cuando pos >0 (getPos(…)retorna el nodo dada una posición)
                 * Ubico antes getPos(pos-1) antes Borrar=siguiente de antes
                 * Antes su siguiente ahora ES el siguiente de borrar Borrar
                 * referencia de nodo borrado, es decir, DESUNIRLO Actualizar
                 * cardinalidad
                 *
                 */

                Nodo<T> antes = this.getPos(pos - 1);
                borrar = antes.getSig();
                antes.setSig(borrar.getSig());

            } catch (Exception ex) {
                System.err.println(ex.getMessage());
                return null;
            }
        }
        borrar.setSig(null);
        this.cardinalidad--;
        return borrar.getInfo();
    }

    
    /**
     * Pasa el nodo con información del objeto info, al final de la lista
     *
     * @param info objeto que deseo mover
     */
    public void cortarDato_pasarlo_Al_Final(T info) {
        //Si la lista es vacía   
        if (this.esVacia() || this.cardinalidad == 1) {
            System.err.println("No se puede cortar");
        }
        //Si info no está en Lista  <-- ??????????? 
        if (!this.esta(info)) {
            System.err.println("No se encuentra en la lista");;
        } else {
            try {

                /*Nodo<T> borrar = null;
                Nodo<T> nuevo = this.getPos(cardinalidad - 1);
                nuevo=borrar;
                Nodo<T> antes = this.getPos(pos-1);
                
                borrar = antes.getSig();
                antes.setSig(borrar.getSig());
                nuevo.setSig(null);
                this.cardinalidad++;
                 */
            } catch (Exception ex) {
                System.err.println(ex.getMessage());

            }

        }
    }
    
    
    private Nodo<T>[] getPosiciones(T info) {
        // vector-> De tres posiciones: <anterior, actual,último>
        Nodo<T>[] posiciones = new Nodo[3];

        // el problema es cuando info es la cabeza       
        if (this.cabeza.getInfo().equals(info)) {
            posiciones[0] = posiciones[1] = this.cabeza;
        }
        for (Nodo<T> x = this.cabeza; x != null; x = x.getSig()) {
            //proceso es válido si y solo el info no está en la cabeza
            if (x.getSig() != null && x.getSig().getInfo().equals(info)) {
                posiciones[0] = x;
                posiciones[1] = x.getSig();
            }
            if (x.getSig() == null) {
                posiciones[2] = x;
            }

        }
        return posiciones;
    }

    
    public void ordenarInsercionPorInfos() throws Exception {

        long inicio = System.currentTimeMillis();

        if (this.esVacia() || this.cardinalidad == 1) {
            throw new Exception("No es posible ordenar la lista");
        }

        Nodo<T> aux1 = this.cabeza;
        Nodo<T> aux2 = aux1.getSig();

        while (aux2 != null) {
            if (comparar(aux1.getInfo(), aux1.getSig().getInfo()) > 0) {
                T camb = aux1.getInfo();
                aux1.setInfo(aux1.getSig().getInfo());
                aux1.getSig().setInfo(camb);

                if (aux1 != this.cabeza) {
                    aux1 = getAnterior(aux1);
                }
            } else {
                aux2 = aux2.getSig();
                aux1 = getAnterior(aux2);
            }
        }

        long fin = System.currentTimeMillis();
        double tiempo = (double) ((fin - inicio) / 1000);
        System.out.println("Tiempo de ejecución de inserción por infos: -> " + tiempo + " segundos");
    }

    
    public void ordenarInsercionPorNodos() throws Exception {
        long inicio = System.currentTimeMillis();

        if (this.esVacia() || this.cardinalidad == 1) {
            throw new Exception("No es posible ordenar la lista");
        }

        Nodo<T> aux1 = this.cabeza;
        Nodo<T> aux2 = null;

        while (aux1 != null) {
            Nodo<T> auxSig = aux1.getSig();

            if (aux2 == null || comparar(aux2.getInfo(), aux1.getInfo()) > 0) {
                aux1.setSig(aux2);
                aux2 = aux1;
            } else {
                Nodo<T> Ordenado = aux2;

                while (Ordenado.getSig() != null && comparar(Ordenado.getSig().getInfo(), aux1.getInfo()) < 0) {
                    Ordenado = Ordenado.getSig();
                }

                aux1.setSig(Ordenado.getSig());
                Ordenado.setSig(aux1);
            }
            aux1 = auxSig;

        }
        this.cabeza = aux2;

        long fin = System.currentTimeMillis();
        double tiempo = (double) ((fin - inicio) / 1000);
        System.out.println("Tiempo de ejecución de inserción por nodos: -> " + tiempo + " segundos");
    }

    

    public Nodo<T> getAnterior(Nodo<T> aux) throws Exception {

        if (this.esVacia() || aux == this.cabeza) {
            throw new Exception("No es posible obtener el nodo anterior");
        }

        Nodo<T> temp = this.cabeza;
        while (temp.getSig() != aux) {
            temp = temp.getSig();
        }

        return temp;
    }

    
    public int comparar(T info1, T info2) {

        Comparable comparable = (Comparable) info1;
        return comparable.compareTo(info2);
    }

}
