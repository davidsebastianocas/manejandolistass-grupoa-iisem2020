/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.Modelo.Persona;
import ufps.util.coleciones_seed.ListaCD;

/**
 *
 * @author ACER
 */
public class TestPruebaPalindrome {
    
    
    public static void main(String[] args) throws Exception  {
        ListaCD<Persona> lista1= new ListaCD();
        ListaCD<Integer> lista2 = new ListaCD();
        
        lista1.insertarInicio(new Persona(1,"madarme"));
        lista1.insertarInicio(new Persona(3,"gederson"));
        lista1.insertarInicio(new Persona(4,"diana"));
        lista1.insertarInicio(new Persona(3,"gederson"));
        lista1.insertarInicio(new Persona(1,"madarme"));
        
        lista2.insertarInicio(3);
        lista2.insertarInicio(2);
        lista2.insertarInicio(1);
        lista2.insertarInicio(2);
        lista2.insertarInicio(3);
        
        System.out.println(lista1.esPalindroma());
        System.out.println(lista2.esPalindroma());
    }
    
}
