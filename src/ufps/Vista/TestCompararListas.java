/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.util.coleciones_seed.ListaCD;
import ufps.util.coleciones_seed.ListaS;

/**
 *
 * @author madar
 */
public class TestCompararListas {
    
    public static void main(String[] args) {
        
        int n=5000000;
        //crearListaSimple(n);
        crearListaCD(n);
    }
    
    
    private static void crearListaSimple(int n)
    {
        long inicio = System.currentTimeMillis();
        ListaS<Integer> l=new ListaS();
        
        for(int i=1;i<=n;i++)
            l.insertarFinal(i);
        
        long fin = System.currentTimeMillis();
        double tiempo = (double) ((fin - inicio)/1000);
        
        System.out.println("El tiempo de insertarFIn en ListaS:"+tiempo +" segundos");
    }
    
     private static void crearListaCD(int n)
    {
    long inicio = System.currentTimeMillis();    
    ListaCD<Integer> l=new ListaCD();
    
        for(int i=1;i<=n;i++)
            l.insertarFin(i);
        
    long fin = System.currentTimeMillis();
    double tiempo = (double) ((fin - inicio)/1000);
    System.out.println("El tiempo de insertarFIn en ListaCD:"+tiempo +" segundos");    
    }
}
