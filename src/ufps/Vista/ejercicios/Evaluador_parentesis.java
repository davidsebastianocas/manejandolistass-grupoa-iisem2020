/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista.ejercicios;

import java.util.Scanner;

import ufps.util.coleciones_seed.*;

/**
 *
 * @author madar
 */
// (9+5)*((4*3)
public class Evaluador_parentesis {

    public static void main(String[] args) {
        String cadena;
        System.out.println("Digite una cadena con ()");
        Scanner teclado = new Scanner(System.in);
        cadena = teclado.next();
        System.out.println("Evaluando sus paréntesis: " + evaluador(cadena));

    }

    private static boolean evaluador(String cadena) {

        Pila<Character> pilaAux = new Pila();
        
        
        for (int cont = 0; cont < cadena.length(); cont++) {

            if(cadena.charAt(cont)==')')
            {
                System.err.println("aunque puede que haya paridad de parentesis, la expresión matemática es erronea :( ");
            break;
            
            }else 
            if (cadena.charAt(cont) == '(') {
                pilaAux.push('(');
            } else if (!pilaAux.esVacia() && cadena.charAt(cont) == ')') {
                pilaAux.pop();
            }

        }
        

        if (pilaAux.esVacia()) {
            return true;
        }

        return false;
    }

}
