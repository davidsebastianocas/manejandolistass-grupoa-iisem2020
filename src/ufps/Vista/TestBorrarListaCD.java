/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import ufps.Modelo.Persona;
import ufps.util.coleciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class TestBorrarListaCD {
    public static void main(String[] args) {
        ListaCD<Persona> personas=new ListaCD();
        personas.insertarInicio(new Persona(1,"madarme"));
        personas.insertarInicio(new Persona(3,"gederson"));
        personas.insertarInicio(new Persona(4,"diana"));
         personas.insertarInicio(new Persona(5,"carlos"));
        System.out.println(personas);
        
        System.out.println("Borrando a gederson:"+personas.eliminar(2));
        System.out.println(personas);
        
    }
}
