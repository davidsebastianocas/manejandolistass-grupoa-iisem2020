/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.Vista;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import ufps.util.coleciones_seed.ListaS;

/**
 *
 * @author madar
 */
public class TestLista_Borrar {

    public static void main(String[] args) throws Exception {

        int variableCantidad = 5000000;
        System.out.println("experimentos realizados con los valores de: -> " +variableCantidad + "  datos");
        System.out.println("");
        System.out.println("DATOS ESCALONADOS: \n");
//        experimentos_1_2_3_Infos(variableCantidad);
        experimentos_1_2_3_Nodos(variableCantidad);
        System.out.println("");
        System.out.println("DATOS ALEATORIOS: \n");
//        experimentos_4_5_6_Infos(variableCantidad);
//        experimentos_4_5_6_Nodos(variableCantidad);

    }
    
    public static void experimentos_1_2_3_Nodos(int variableCantidad) throws Exception
    {
        ListaS<Integer> enteros = new ListaS();
        for (int i = 0; i <variableCantidad; i++) {
           enteros.insertarInicio(i);
        }
        
        enteros.ordenarInsercionPorNodos();
         
         System.out.println("LISTA ORDENADA: \n");
        System.out.println(enteros);
    }
    
    public static void experimentos_4_5_6_Nodos(int variableCantidad) throws Exception
    {
        ListaS<Integer> enteros = new ListaS();
        for (int i = 0; i < variableCantidad; i++)
        {
            enteros.insertarInicio((int)(Math.random()*variableCantidad));
        }
        
         enteros.ordenarInsercionPorNodos();
         
         System.out.println("LISTA ORDENADA: \n");
        System.out.println(enteros);
    }
    
    public static void experimentos_1_2_3_Infos(int variableCantidad) throws Exception
    {
        ListaS<Integer> enteros = new ListaS();
        for (int i = 0; i <variableCantidad; i++) {
           enteros.insertarInicio(i);
        }
        
         enteros.ordenarInsercionPorInfos();
         
         System.out.println("LISTA ORDENADA: \n");
        System.out.println(enteros);
    }
    
    public static void experimentos_4_5_6_Infos(int variableCantidad) throws Exception
    {
        ListaS<Integer> enteros = new ListaS();
        for (int i = 0; i < variableCantidad; i++)
        {
            enteros.insertarInicio((int)(Math.random()*variableCantidad));
        }
        
         enteros.ordenarInsercionPorInfos();
         
         System.out.println("LISTA ORDENADA: \n");
        System.out.println(enteros);
    }
}
